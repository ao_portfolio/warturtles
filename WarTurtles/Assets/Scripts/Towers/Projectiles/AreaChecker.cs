using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaChecker : MonoBehaviour
{
    public SphereCollider range;
    public List<GameObject> unitsInArea = new List<GameObject>();

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.parent.CompareTag("Unit"))
        {
            unitsInArea.Add(collider.transform.parent.gameObject);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.transform.parent.CompareTag("Unit"))
        {
            unitsInArea.Remove(collider.transform.parent.gameObject);
        }
    }
}
