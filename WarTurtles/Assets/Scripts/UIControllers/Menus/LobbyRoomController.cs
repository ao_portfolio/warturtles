using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/**
 * 
 * Controller for the Lobby Room UI
 * 
 * From the lobby you can either create or join a room.
 * Or you can disconnect and return back to the main menu
 * 
 * RoomOptions
 *  MaxPlayers: 4
 *  EmptyRoomTtl = 0;
 *  PlayerTtl = 0;
 *  
 * */

public class LobbyRoomController : MonoBehaviourPunCallbacks {
    [SerializeField] private Button disconnectFromServerButton;
    [SerializeField] private Button createRoomButton;
    [SerializeField] private Button joinRoomButton;

    [SerializeField] private InputField createRoomInput;
    [SerializeField] private InputField joinRoomInput;

    [SerializeField] private Text usernameField;

    void Start() {
        if (!PhotonNetwork.IsConnected) {
            SceneManager.LoadScene(0);
        }
        usernameField.text = "Welcome, " + PhotonNetwork.NickName;

        joinRoomButton.gameObject.SetActive(false);
        createRoomButton.gameObject.SetActive(false);
    }

    void Update() {
        // Only enable disconnect & join/create room buttons when client is ready
        disconnectFromServerButton.gameObject.SetActive(PhotonNetwork.IsConnected);
        joinRoomButton.gameObject.SetActive(PhotonNetwork.IsConnectedAndReady);
        createRoomButton.gameObject.SetActive(PhotonNetwork.IsConnectedAndReady);
    }

    public void OnDisconnectFromServerButton() {
        PhotonNetwork.Disconnect();
        SceneManager.LoadSceneAsync(0);
    }

    public void OnCreateRoomButton() {
        if (PhotonNetwork.NetworkClientState == ClientState.JoiningLobby) {
            return;
        }
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 4;
        roomOptions.EmptyRoomTtl = 0;
        roomOptions.PlayerTtl = 0;
        PhotonNetwork.CreateRoom(createRoomInput.text, roomOptions);
        SceneManager.LoadScene(2);
    }

    public void OnJoinRoomButton() {
        if (string.IsNullOrEmpty(joinRoomInput.text) ||
            PhotonNetwork.NetworkClientState == ClientState.JoiningLobby)
        {
            return;
        }
        // Loads waiting room in the background
        PhotonNetwork.JoinRoom(joinRoomInput.text);
        SceneManager.LoadScene(2);
    }
}
