public interface IGameState {
    IGameState DoState(GameController game);
}
