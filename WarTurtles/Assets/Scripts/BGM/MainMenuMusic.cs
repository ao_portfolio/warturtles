using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuMusic : MonoBehaviour {
    private static MainMenuMusic music;

    private void Awake() {
        if (music != null && music != this) {
            Destroy(gameObject);
        }
        else {
            music = this;
            DontDestroyOnLoad(gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        Debug.Log(scene.name);
        if (scene.name.Contains("Level")) {
            Destroy(GameObject.Find("BGM Menu"));
        }
    }
}
