using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundTimer : MonoBehaviour {
    public Timer timer;

    // Start is called before the first frame update
    private void Start() {
        timer = gameObject.GetComponentInChildren<Timer>();
    }

    public void StartTimer(int minutes, int seconds) {
        timer.minutes = minutes;
        timer.seconds = seconds;
        timer.StartTimer();
    }
}
