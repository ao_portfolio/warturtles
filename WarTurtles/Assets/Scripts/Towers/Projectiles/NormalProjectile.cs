using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalProjectile : Projectile
{
    protected override void OnCollisionEnter(Collision collision)
    {
        if (collision.collider != null && collision.collider.CompareTag("Unit"))
        {
            collision.collider.transform.parent.GetComponent<Unit>().Hit(damage);
        }
        PhotonNetwork.Destroy(gameObject);
    }
}
