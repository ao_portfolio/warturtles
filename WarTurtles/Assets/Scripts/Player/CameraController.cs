using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float deadzone = 100f;
    [SerializeField] private float maxCameraSpeed = 3f;
    [SerializeField] private float cameraTurnSpeed = 100f;
    [SerializeField] private float fov = 70;
    [SerializeField] private bool enableMouseMovement = true;
    [SerializeField] private float minCameraHeight = 2f;
    [SerializeField] private float maxCameraHeight = 35f;

    private Vector2 screenSize;

    void Start() {
        GetComponent<Camera>().enabled = true;

        SetCameraSettings();
        SetScreenSize();
    }

    void FixedUpdate() {
        SetScreenSize();

        if (MouseOutOfBounds()) return;

        // Camera movement
        float cameraSpeed = GetCameraSpeed() * Time.deltaTime;

        Zoom(cameraSpeed);
        Pan(cameraSpeed);
        Rotate(cameraSpeed);
    }

    private void Zoom(float cameraSpeed) {
        float cameraZoom = Input.GetAxis("Mouse ScrollWheel");

        transform.position += Quaternion.Euler(transform.rotation.eulerAngles) * Vector3.forward * cameraZoom * cameraSpeed * 10;
        LimitZoom();
    }

    private void Pan(float cameraSpeed) {
        bool cameraLeft = Input.GetKey(KeyCode.Q);
        bool cameraRight = Input.GetKey(KeyCode.D);
        bool cameraForward = Input.GetKey(KeyCode.Z);
        bool cameraBack = Input.GetKey(KeyCode.S);

        if (enableMouseMovement && !EventSystem.current.IsPointerOverGameObject()) {
            cameraLeft = cameraLeft || Input.mousePosition.x < deadzone;
            cameraRight = cameraRight || Input.mousePosition.x > screenSize.x - deadzone;
            cameraForward = cameraForward || Input.mousePosition.y > screenSize.y - deadzone;
            cameraBack = cameraBack || Input.mousePosition.y < deadzone;
        }

        if (cameraLeft) {
            transform.position += Quaternion.Euler(transform.rotation.eulerAngles) * Vector3.left * cameraSpeed;
        }
        if (cameraRight) {
            transform.position += Quaternion.Euler(transform.rotation.eulerAngles) * Vector3.right * cameraSpeed;
        }
        if (cameraForward) {
            transform.position += Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f) * Vector3.forward * cameraSpeed;
        }
        if (cameraBack) {
            transform.position += Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f) * Vector3.back * cameraSpeed;
        }
    }

    private void Rotate(float cameraSpeed) {
        bool cameraRotateLeft = Input.GetKey(KeyCode.A);
        bool cameraRotateRight = Input.GetKey(KeyCode.E);

        if (cameraRotateLeft) {
            transform.Rotate(0f, -cameraTurnSpeed * Time.deltaTime, 0f, Space.World);
        }
        if (cameraRotateRight) {
            transform.Rotate(0f, cameraTurnSpeed * Time.deltaTime, 0f, Space.World);
        }
    }

    private void LimitZoom() {
        float cameraGroundDistance = GetCameraGroundDistance();

        // Makes sure the camera doesn't zoom too far or too close
        if (cameraGroundDistance < minCameraHeight + 1f) {
            //transform.position = new Vector3(transform.position.x, minCameraHeight + 0.5f, transform.position.z);
            transform.position += Vector3.up * Time.deltaTime;
        }
        if (cameraGroundDistance > maxCameraHeight - 1f) {
            //transform.position = new Vector3(transform.position.x, maxCameraHeight - 0.5f, transform.position.z);
            transform.position += Vector3.down * Time.deltaTime;
        }
    }

    private bool MouseOutOfBounds() {
        return Input.mousePosition.x < 0 || Input.mousePosition.y < 0 || Input.mousePosition.x > screenSize.x || Input.mousePosition.y > screenSize.y;
    }

    private float GetCameraSpeed() {
        float groundDistance = GetCameraGroundDistance();

        // Changzes the speed of the camera based on the distance to the ground
        return 2f + (-maxCameraSpeed / 9) * (groundDistance - minCameraHeight) * (groundDistance - maxCameraHeight);
    }

    private float GetCameraGroundDistance() {
        float cameraHeight = transform.position.y;
        LayerMask terrainMask = 1 << 3;

        Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, 1000f, terrainMask);

        float groundDistance = Vector3.Distance(new Vector3(0f, cameraHeight, 0f), new Vector3(0f, hit.point.y, 0f));

        return groundDistance;
    }

    private void SetScreenSize() {
        if (screenSize.x != Screen.width || screenSize.y != Screen.height) {
            screenSize.x = Screen.width;
            screenSize.y = Screen.height;
        }
    }

    private void SetCameraSettings() {
        GetComponent<Camera>().fieldOfView = fov;
    }

    public void DebugRay() {
        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;

        Debug.DrawRay(transform.position, forward, Color.green);
        Debug.DrawRay(transform.position, Vector3.forward * 10, Color.red);
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * 10, Color.blue);
    }
}
