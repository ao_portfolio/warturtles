using System.Linq;

public class PreparationState : IGameState {
    public IGameState DoState(GameController game) {
        if (game.Round > GameController.MAX_ROUNDS) {
            game.SendGameInfo(game.LoseState);
            return game.LoseState;
        }

        if (game.PlayersInRoom.All(p => Helpers.GetPlayerReadyStatus(p))) {
            foreach (var player in game.PlayersInRoom) {
                Helpers.SetCustomPlayerAttribute(player, "ready", false);
            }
            game.ActivateTowers();
            game.ActivateUnits();
            game.GameTimer.StartTimer(2,0);
            game.SendGameInfo(game.BattleState);

            return game.BattleState;
        }
        return game.PreparationState;
    }

    public override string ToString() {
        return "PreparationState";
    }
}
