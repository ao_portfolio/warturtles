using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitState : IGameState {

    public IGameState DoState(GameController game) {
        game.transform.Find("GameTimer").GetComponent<Canvas>().enabled = true;

        game.PlayersInRoom = PhotonNetwork.PlayerList;
        game.RoomName = PhotonNetwork.CurrentRoom.Name;
        game.Map = SceneManager.GetActiveScene().name;
        game.Round = 1;
        game.CastleHealth = 100f;
        game.RoundOver = false;
        game.GameOver = false;
        game.GameTimer = game.GetComponentInChildren<RoundTimer>();
        //Chat = GameObject.Find("Chat").GetComponent<GameChatController>();
        //Chat.InitChat();

        game.SendGameInfo(game.InitState);
        game.DeactivateUnits();

        SetRandomCheckpoint();
        SetTowers(game);

        if (game.Players.Length == game.PlayersInRoom.Length) {
            // All players loaded in game scene
            game.SendGameInfo(game.PreparationState);
            game.DeactivateUnits();

            if (game.Towers.Values.Where(v => v != null).Count() == 0) {
                game.SpawnTowers();
            }

            // Go to prepare phase
            return game.PreparationState;
        }
        else {
            // Remain in init phase to load in other players
            game.GetPlayers();
            return game.InitState;
        }
    }

    public void SetRandomCheckpoint() {
        var paths = GameObject.Find("Paths").GetComponentsInChildren<WaypointsController>();
        var waypoints = GameObject.Find("Paths").GetComponentsInChildren<Waypoint>();

        // Check if a checkpoint already exists on the map
        if (waypoints.Any(waypoint => waypoint.IsCheckpoint)) {
            return;
        }

        // Get random path and its waypoints
        int randomPathIndex = UnityEngine.Random.Range(0, paths.Length);
        var randomPath = paths[randomPathIndex];
        var randomPathWaypoints = randomPath.GetComponentsInChildren<Waypoint>();

        // Get random waypoint in path, excluding last one (castle waypoint)
        int randomWaypointIndex = UnityEngine.Random.Range(0, randomPathWaypoints.Length - 1);
        var randomWaypoint = waypoints[randomWaypointIndex];
        randomWaypoint.IsCheckpoint = true;
    }

    private void SetTowers(GameController game) {
        if (game.Towers != null) {
            return;
        }
        var locations = GameObject.Find("Locations").transform;
        var towers = new Dictionary<TowerLocation, Tower>();

        foreach (TowerLocation location in locations.GetComponentsInChildren<TowerLocation>()) {
            location.startTower = SetTowerType(location);
            towers.Add(location, null);
        }

        game.Towers = towers;
    }

    // Set a random towerType for a given location if it is not set yet
    private TowerType SetTowerType(TowerLocation location) {
        if (location.startTower != TowerType.None) {
            return location.startTower;
        } else {
            int min = 0;
            int max = Enum.GetValues(typeof(TowerType)).Length - 1;
            float bias = 2.1f;
            float randomNumber = UnityEngine.Random.Range(0f, 0.99f);

            /*
             * To get more normal towers than poison or slow towers
             * a function is used to bias more towards lower values
             * The function used is a power function
             * 
             * y = ax^n + b
             * a: steepness
             * x: parameter (random number)
             * b: intersection with y-axis
             * n: bias (power)
             * 
             * y = (max-min) randomNumber^(bias) + min
             * 
            */
            int randomIndex = Mathf.FloorToInt((max - min) * Mathf.Pow(randomNumber, bias) + min);
            TowerType towerType = (TowerType)randomIndex;
            return towerType;
        }
    }

    public override string ToString() {
        return "InitState";
    }
}
