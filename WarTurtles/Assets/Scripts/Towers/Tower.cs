using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public Projectile projectile;
    protected GameObject target = null;
    public GameObject partToRotate;
    public SphereCollider range;
    public float attackSpeed = 10f;
    public float attackDamage = 3f;
    public float attackRange = 5f;
    public bool disabled = false;

    protected List<GameObject> _unitsInRange = new List<GameObject>();

    // Start is called before the first frame update
    protected virtual void Start()
    {
        range.radius = attackRange;
        InvokeRepeating(nameof(Shoot), 0, 10 / attackSpeed);
    }

    protected virtual void Update()
    {
        if(target != null) Rotation();
    }

    protected virtual void Shoot()
    {
        if (target != null && !disabled && target.GetComponent<Unit>().targetable)
        {
            Debug.Log($"Shooting {projectile.GetType()}");
            var newProjectile = PhotonNetwork.Instantiate("Towers/Projectiles/" + projectile.GetType(), projectile.transform.position, projectile.transform.rotation);
            newProjectile.gameObject.SetActive(true);
            newProjectile.transform.SetParent(partToRotate.transform);
            newProjectile.GetComponent<Projectile>().damage = attackDamage;
            newProjectile.GetComponent<Projectile>().force *= TargetDistance();
        }
        else if(_unitsInRange.Count > 0 && !disabled)
        {
            _unitsInRange = _unitsInRange.Where(item => item != null).ToList();
            NextTarget();
        }
    }

    protected float TargetDistance()
    {
        return (float)Math.Sqrt(Math.Pow(transform.position.x - target.transform.position.x, 2f) +  Math.Pow(transform.position.z - target.transform.position.z, 2f));
    }

    protected virtual void OnTriggerEnter(Collider collider)
    {
        if (collider.transform == null) return;
        if (collider.transform.parent.CompareTag("Unit"))
        {
            _unitsInRange.Add(collider.transform.parent.gameObject);
            if (target == null)
            {
                target = _unitsInRange[0];
            }
        }
    }

    protected virtual void OnTriggerExit(Collider collider)
    {
        if (collider.transform.parent.CompareTag("Unit"))
        {
            _unitsInRange.Remove(collider.transform.parent.gameObject);
            NextTarget();
        }
    }

    protected void NextTarget()
    {
        if(_unitsInRange.Count == 0)
        {
            target = null;
        }
        else
        {
            target = _unitsInRange[0];
        }
    }

    protected void Rotation()
    {
        var direction = target.transform.position - transform.position;
        var lookRotation = Quaternion.LookRotation(direction);
        //var rotation = lookRotation.eulerAngles;
        //For smooth rotation:
        var rotation = Quaternion.Lerp(partToRotate.transform.rotation, lookRotation, Time.deltaTime * 10).eulerAngles;
        partToRotate.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }
}
