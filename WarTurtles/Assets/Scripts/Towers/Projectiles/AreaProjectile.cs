using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaProjectile: Projectile
{
    public float range = 5;
    public AreaChecker ac;
    public GameObject explosion;
    protected override void Start()
    {
        ac.range.radius = range;
        
        rb.AddRelativeForce(0, 400, force);
        transform.position = GetComponentInParent<Transform>().position;
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        foreach (var unit in ac.unitsInArea)
        {
            if(unit != null)
            {
                unit.GetComponent<Unit>().Hit(damage);
            }
        }
        explosion.transform.SetParent(transform.parent);
        explosion.SetActive(true);
        PhotonNetwork.Destroy(gameObject);
    }
}

