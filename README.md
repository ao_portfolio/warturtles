# War Turtles: Anti-tower defence game with Unity

### Other names
Project Anti-Tower Defence, Superwaffle

### Authors
[Margaux Meuleman](https://gitlab.com/margaux.meuleman1), [Aaron Odent](https://gitlab.com/aaron.odent), [Sander Van Houtte](https://gitlab.com/sander.vanhoutte), [Jonathan Vercammen](https://gitlab.com/jonathan.vercammen)

## Description
The goal of this project is to develop a functional ‘Anti-Tower Defence Game’. In essence, this means that the player attacks a location which the computer defends by placing towers with projectiles. This is done in a multiplayer context of up to four players who work together to defeat the computer. The creation of this game happens in Unity using C# for scripting.