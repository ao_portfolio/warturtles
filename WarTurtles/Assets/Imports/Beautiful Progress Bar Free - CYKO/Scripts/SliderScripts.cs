﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;



public class SliderScripts : MonoBehaviour
{
    public Slider Slider;
    public Image Fill;
    public TMP_Text CastleHealthText;

    private int castleHealth;
    private void Start() {

    }

    private void Update() {
    }

    public void FillSlider() {
        Fill.fillAmount = castleHealth / 100;
    }

    public void SetCastleHealthText() {
        CastleHealthText.text = "Castle Health: " + castleHealth + "%";
    }
}
