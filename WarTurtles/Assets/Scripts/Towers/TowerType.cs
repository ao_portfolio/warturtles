public enum TowerType {
    None = -1,
    Normal = 0,
    Area = 1,
    Poison = 2,
    Slow = 3
}
