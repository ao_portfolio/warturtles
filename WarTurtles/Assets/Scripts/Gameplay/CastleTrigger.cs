using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleTrigger : MonoBehaviour {

    public void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Unit")) {
            if (PhotonNetwork.IsMasterClient) {
                var game = GameObject.Find("gameplay").GetComponent<GameController>();
                float damage = other.GetComponentInParent<Unit>().damage;
                game.CastleHealth -= damage;
                game.CastleHealth = game.CastleHealth < 0 ? 0 : game.CastleHealth;

                Helpers.SetCustomRoomAttribute("castleHealth", game.CastleHealth);
            }
        }
    }

}
