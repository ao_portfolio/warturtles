using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class Helpers : ScriptableObject {
    // SETTERS
    public static void SetCustomPlayerAttribute(Player player, string attribute, object value) {
        //Debug.Log("Setting " + attribute + " to " + value.ToString() + " for " + player.NickName);

        Hashtable attributes = new Hashtable();
        attributes.Add(attribute, value);

        player.SetCustomProperties(attributes);
    }

    public static void SetCustomRoomAttribute(string attribute, object value) {
        Hashtable attributes = new Hashtable();
        attributes.Add(attribute, value);

        PhotonNetwork.CurrentRoom.SetCustomProperties(attributes);
    }
    // GETTERS
    public static string GetPlayerName(Player player) {
        return player.NickName ?? "Unknown";
    }

    public static Color GetPlayerColor(Player player) {
        try {
            Color playerColor;
            ColorUtility.TryParseHtmlString(GetPlayerColorHex(player), out playerColor);
            return playerColor;
        } catch {
            return Color.white;
        }
    }

    public static TDPlayerController GetPlayerController(Player player) {
        try {
            Transform players = GameObject.Find("Players").transform;
            return players.Find(player.NickName).GetComponent<TDPlayerController>();
        } catch {
            return null;
        }
    }

    public static HudController GetPlayerHud(Player player) {
        try {
            Transform players = GameObject.Find("Players").transform;
            return players.Find(player.NickName).GetComponent<HudController>();
        }
        catch {
            return null;
        }
    }

    public static bool GetPlayerReadyStatus(Player player) {
        try {
            return (bool)player.CustomProperties["ready"];
        } catch {
            return false;
        }
    }

    public static PlayerClass GetPlayerClass(Player player) {
        try {
            return (PlayerClass)System.Enum.Parse(typeof(PlayerClass), player.CustomProperties["class"].ToString());
        } catch {
            return PlayerClass.WARRIOR;
        }
    }

    public static Texture GetPlayerClassIcon(Player player) {
        var playerClass = GetPlayerClass(player);
        return Resources.Load("Icons/Classes/" + playerClass.ToString()) as Texture;
    }

    public static int GetPlayerGold(Player player) {
        var gold = player.CustomProperties["gold"] ?? 0;
        return (int)gold;
    }


    public static float GetCastleHealth() {
        float castleHealth = (float)PhotonNetwork.CurrentRoom.CustomProperties["castleHealth"];
        return castleHealth < 0 ? 0 : castleHealth;
    }

    public static string GetCurrentMap() {
        string map = (string)PhotonNetwork.CurrentRoom.CustomProperties["selectedMap"];
        return map ?? "Unkown";
    }

    // MISC
    public static string GetPlayerColorString(Player player) {
        string colorString = player.CustomProperties["color"].ToString();

        return colorString;
    }

    public static string GetPlayerColorHex(Player player) {
        string colorString = GetPlayerColorString(player);
        string hexColor = GetHexValueFromString(colorString);

        return hexColor;
    }

    public static string GetHexValueFromString(string colorString) {
        colorString = colorString.Replace("RGBA(", "");
        colorString = colorString.Replace(")", "");
        string[] rgbaString = colorString.Split(',');
        float[] rgba = rgbaString.Select(f => !string.IsNullOrEmpty(f) ? float.Parse(f.Replace('.', ',')) : 1f).ToArray(); // LINQ

        Color color = new Color(rgba[0], rgba[1], rgba[2], rgba[3]);
        string hexColor = ColorUtility.ToHtmlStringRGBA(color);

        return "#" + hexColor;
    }
}
