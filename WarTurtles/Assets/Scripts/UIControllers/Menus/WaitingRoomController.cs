using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/**
 * (WIP)
 * Controller for the Waiting Room UI
 * 
 * This is where players go after they joined or created a room
 * 
 * The name of the room is displayed at the top
 * 
 * On the left is an overview of all players in the room 
 * containing their username, ready status and host if player is host
 * 
 * On the right you can choose a map
 * Currently only for host. In the future maybe a voting system
 * 
 * After everyone is ready, the host can start the game 
 * and all players will go the the selected level scene
 * 
 * A player can also leave the room and return back to the main menu
 * 
 * While everyone waits, you can chat with eachother at the left side
 * */

public class WaitingRoomController : MonoBehaviourPunCallbacks {
    [SerializeField] private Button leaveRoomButton;
    [SerializeField] private Button readyButton;
    [SerializeField] private Button startButton;

    [SerializeField] private GameObject playerList;
    [SerializeField] private GameObject mapList;
    [SerializeField] private GameObject classList;
    [SerializeField] private GameObject mapIcon;
    [SerializeField] private Text roomNameField;
    [SerializeField] private Text playerNamePrefab;

    [SerializeField] private GameChatController chatController;



    void Start() {
        roomNameField.text = "Joining...";
        if (!PhotonNetwork.IsConnected) {
            SceneManager.LoadScene(0);
        }

        if (PhotonNetwork.InRoom) {
            InitWaitingRoom();
        }
    }

    private void InitWaitingRoom() {
        roomNameField.text = PhotonNetwork.CurrentRoom.Name;
        roomNameField.text += " | " + PhotonNetwork.LocalPlayer.NickName;

        chatController.InitChat();
        UpdatePlayerList();
        UpdateMapList();
    }

    #region Buttons
    public void OnLeaveRoomButton() {
        chatController.LeaveChat();
        PhotonNetwork.LeaveRoom();
    }

    public void OnReadyButton() {
        Player localPlayer = PhotonNetwork.LocalPlayer;
        Helpers.SetCustomPlayerAttribute(localPlayer, "ready", !Helpers.GetPlayerReadyStatus(localPlayer));
    }

    public void OnStartButton() {
        if (PhotonNetwork.IsMasterClient) {
            if (PhotonNetwork.PlayerList.All(p => Helpers.GetPlayerReadyStatus(p))) {
                foreach (var player in PhotonNetwork.PlayerList) {
                    Helpers.SetCustomPlayerAttribute(player, "gold", 100);
                    Helpers.SetCustomPlayerAttribute(player, "ready", false);
                }
                Helpers.SetCustomRoomAttribute("castleHealth", 100f);

                var map = GetSelectedMap();
                PhotonNetwork.LoadLevel(map);
            } else {
                Debug.Log("Not all players are ready");
            }
        }
    }

    public void OnSelectedClass(string className) {
        Helpers.SetCustomPlayerAttribute(PhotonNetwork.LocalPlayer, "class", className);
        HighlightClass(className);
    }
    #endregion

    private void UpdatePlayerList() {
        // Clear list except for title
        foreach (Transform item in playerList.transform) {
            if (item == playerList.transform.GetChild(0)) {
                continue;
            }
            GameObject.Destroy(item.gameObject);
        }

        const string tabs = "\t\t\t";
        foreach (var player in PhotonNetwork.PlayerList) {
            string hexColor = Helpers.GetPlayerColorHex(player);
            bool readyStatus = Helpers.GetPlayerReadyStatus(player);

            string playerInfo = $"<color={hexColor}>" + player.NickName + "</color>";
            playerInfo += player.IsMasterClient ? tabs + "(Host)" : tabs + tabs;
            playerInfo += readyStatus ? tabs + "Ready" : "";

            Text playerName = Instantiate(playerNamePrefab, playerList.transform);
            playerName.text = playerInfo;
        }
    }

    private void UpdateMapList() {
        // Clear list
        foreach (Transform item in mapList.transform) {
            Destroy(item.gameObject);
        }
        for (int i = 0; i < 2; i++) {
            GameObject map = Instantiate(mapIcon, mapList.transform);
            map.name = "Level" + (i + 1);
            Texture icon = Resources.Load($"Icons/Maps/{map.name}", typeof(Texture)) as Texture;

            map.GetComponent<Button>().onClick.AddListener(delegate { OnSelectedMap(map); });
            map.GetComponent<RawImage>().texture = icon;

            if (!PhotonNetwork.IsMasterClient) {
                map.GetComponent<Button>().enabled = false;
            }
        }
        HighlightMap(GetSelectedMap());
    }

    private void OnSelectedMap(GameObject map) {
        if (PhotonNetwork.IsMasterClient) {
            Helpers.SetCustomRoomAttribute("selectedMap", map.name);
        }
    }



    #region PhotonCallbacks
    public override void OnPlayerEnteredRoom(Player newPlayer) {
        base.OnPlayerEnteredRoom(newPlayer);
        UpdatePlayerList();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer) {
        base.OnPlayerLeftRoom(otherPlayer);
        UpdatePlayerList();
        UpdateMapList();
    }

    public override void OnJoinedRoom() {
        base.OnJoinedRoom();
        // When a player succesfully joins the room
        // the room name gets updated and username gets displayed
        // A connection to the chat room is also made
        InitWaitingRoom();
    }

    public override void OnLeftRoom() {
        base.OnLeftRoom();
        // Sets player ready status to false so you don't join a new room as ready
        Helpers.SetCustomPlayerAttribute(PhotonNetwork.LocalPlayer, "ready", false);
        SceneManager.LoadScene(1);
    }

    public override void OnJoinRoomFailed(short returnCode, string message) {
        base.OnJoinRoomFailed(returnCode, message);
        Debug.Log(returnCode + ": " + message);
        SceneManager.LoadScene(1);
    }
    public override void OnCreateRoomFailed(short returnCode, string message) {
        base.OnCreateRoomFailed(returnCode, message);
        Debug.Log(returnCode + ": " + message);
        SceneManager.LoadScene(1);
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps) {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        UpdatePlayerList();
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        HighlightMap(GetSelectedMap());
    }

    #endregion

    #region Helpers
    private string GetSelectedMap() {
        var selectedMap = PhotonNetwork.CurrentRoom.CustomProperties["selectedMap"] + "";
        return string.IsNullOrEmpty(selectedMap) ? "level" + Random.Range(1,3) : selectedMap;
    }

    private void HighlightMap(string selectedMap) {
        if (string.IsNullOrEmpty(selectedMap)) {
            return;
        }
        foreach(Transform map in mapList.transform) {
            if (map.name == selectedMap) {
                map.GetComponentInParent<RawImage>().color = Color.cyan;
            } else {
                map.GetComponentInParent<RawImage>().color = Color.white;
            }
        }
    }

    private void HighlightClass(string selectedClass) {
        if (string.IsNullOrEmpty(selectedClass)) {
            return;
        }
        foreach (Transform playerClass in classList.transform) {
            if (playerClass.name == selectedClass) {
                playerClass.GetComponentInParent<Image>().color = Color.cyan;
            }
            else {
                playerClass.GetComponentInParent<Image>().color = Color.white;
            }
        }
    }
    #endregion
}
