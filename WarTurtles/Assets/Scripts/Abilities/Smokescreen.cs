using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smokescreen : MonoBehaviour
{
    public int duration = 10;
    public int price = 50;
    private List<GameObject> _unitsInRange = new List<GameObject>();
    protected void Start()
    {
        Invoke("Despawn", duration);
    }
    protected virtual void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.parent.CompareTag("Unit"))
        {
            _unitsInRange.Add(collider.transform.parent.gameObject);
            collider.transform.parent.GetComponent<Unit>().targetable = false;
        }
    }

    protected virtual void OnTriggerExit(Collider collider)
    {
        if (collider.transform.parent.CompareTag("Unit"))
        {
            _unitsInRange.Remove(collider.transform.parent.gameObject);
            collider.transform.parent.GetComponent<Unit>().targetable = true;
        }
    }

    private void Despawn()
    {
        foreach(var unit in _unitsInRange)
        {
            unit.GetComponent<Unit>().targetable = true;
        }
        Destroy(gameObject);
    }
}
