using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndState : IGameState {
    public IGameState DoState(GameController game) {
        PhotonNetwork.DestroyAll();
        return game.EndState;
    }
}
