using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour {

    public GameObject normalTower;
    public GameObject areaTower;
    public GameObject poisonTower;
    public GameObject slowTower;

    public GameObject locations;

    public int round = 1;
    private int newTowersPerRound = 3;

    private List<TowerLocation> _emptyLocations = new List<TowerLocation>();
    private List<GameObject> _towers = new List<GameObject>();
    private List<GameObject> _towerPrefabs;

    private Vector3 _locationOffset = new Vector3(0, -1.5f, 0);


    void Start() {
        _towerPrefabs = new List<GameObject>() { normalTower, areaTower, poisonTower, slowTower };
        InitTowers();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyUp(KeyCode.L)) {
            RoundUp();
        }
    }

    private void InitTowers() {
        for (int i = 0; i < locations.transform.childCount; i++) {
            var location = locations.transform.GetChild(i).gameObject.GetComponent<TowerLocation>();
            switch (location.startTower) {
                case TowerType.Normal:
                    _towers.Add(Instantiate(normalTower, location.transform.position + _locationOffset, location.transform.rotation, location.transform.parent.parent));
                    break;
                case TowerType.Area:
                    _towers.Add(Instantiate(areaTower, location.transform.position + _locationOffset, location.transform.rotation, location.transform.parent.parent));
                    break;
                case TowerType.Poison:
                    _towers.Add(Instantiate(poisonTower, location.transform.position + _locationOffset, location.transform.rotation, location.transform.parent.parent));
                    break;
                case TowerType.Slow:
                    _towers.Add(Instantiate(slowTower, location.transform.position + _locationOffset, location.transform.rotation, location.transform.parent.parent));
                    break;
                default:
                    _emptyLocations.Add(location);
                    break;
            }
        }
    }

    private void RoundUp() {
        if (_emptyLocations.Count > 0) //Add random tower to random predefined location if available
        {
            for(int i = 0; i < newTowersPerRound; i++)
            {
                var location = _emptyLocations[Random.Range(0, _emptyLocations.Count)];
                _towers.Add(Instantiate(_towerPrefabs[Random.Range(0, 4)], location.transform.position + _locationOffset, location.transform.rotation, location.transform.parent.parent));
                _emptyLocations.Remove(location);
            }  
        }
        round++;
    }

    private void DisableAllTowers(bool disabled)
    {
        foreach(var tower in _towers)
        {
            tower.GetComponent<Tower>().disabled = disabled;
        }
    }

}
