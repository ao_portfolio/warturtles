using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/**
 * Controller for the Main Menu UI
 * 
 * The main screen has four buttons
 * 1. Connect To Server: Opens the login screen to enter username and player color
 * 2. Credits:           Opens the credit screen (Not implemented yet)
 * 3. Options:           Opens the options screen (Not implemented yet)
 * 4. Quit:              Quits the application
 * 
 * The login screen has two buttons and two inputs for player name and color
 * 1. Play:              Connects to the main photon server with player name (and color)
 *                       and redirects the player to the lobby menu
 * 2. Back:              Returns to the main menu
 * 
 * 
 * */
public class MainMenuController : MonoBehaviourPunCallbacks {
    [SerializeField] private Button connectToServerButton;
    [SerializeField] private Button playButton;
    [SerializeField] private Button creditsButton;
    [SerializeField] private Button optionsButton;
    [SerializeField] private Button quitButton;

    [SerializeField] private InputField playerNameInput;
    [SerializeField] private Image playerColorInput;

    private Canvas loginScreen;
    private Canvas mainMenu;

    private bool isConnecting = false;


    private void Awake() {
        loginScreen = GameObject.Find("Login").GetComponentInChildren<Canvas>();
        mainMenu = GameObject.Find("Main Menu").GetComponentInChildren<Canvas>();
        loginScreen.GetComponent<Canvas>().enabled = false;
    }

    // Update is called once per frame
    private void Update() {
        // To make sure the user doesn't press anything while the game is connecting
        if (isConnecting) {
            mainMenu.GetComponent<Canvas>().enabled = false;
            loginScreen.GetComponent<Canvas>().enabled = false;
        }
    }

    #region Buttons
    public void OnConnectToServerButton() {
        loginScreen.GetComponent<Canvas>().enabled = true;
    }

    public void OnPlayButton() {
        if (string.IsNullOrEmpty(playerNameInput.text)) {
            return;
        }
        isConnecting = true;

        // Connect to Photon
        PhotonNetwork.NickName = playerNameInput.text;
        PhotonNetwork.LocalPlayer.SetCustomProperties(CustomPlayerAttributes());
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.SendRate = 40;
        PhotonNetwork.SerializationRate = 20;
        PhotonNetwork.ConnectUsingSettings();
    }

    public void OnBackButton() {
        loginScreen.GetComponent<Canvas>().enabled = false;
    }

    public void OnCreditsButton() {
    }

    public void OnOptionsButton() {
    }

    public void OnQuitButton() {
        Application.Quit();
    }
    #endregion

    #region PhotonCallbacks
    public override void OnConnectedToMaster() {
        base.OnConnectedToMaster();
        // Load Lobby scene in the background
        PhotonNetwork.JoinLobby();
        SceneManager.LoadSceneAsync(1);
        Debug.Log("Connected to master");
    }

    public override void OnDisconnected(DisconnectCause cause) {
        base.OnDisconnected(cause);
        isConnecting = false;
        Debug.Log("Disconnected");
    }

    #endregion

    // Generate custom player attributes for later use in PhotonNetwork.LocalPlayer
    private Hashtable CustomPlayerAttributes() {
        Hashtable playerAttributes = new Hashtable();

        playerAttributes.Add("color", playerColorInput.color.ToString());
        playerAttributes.Add("ready", false);

        return playerAttributes;
    }
}