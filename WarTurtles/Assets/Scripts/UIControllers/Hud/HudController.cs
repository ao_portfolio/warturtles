using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using System.Linq;
using System;

public class HudController : MonoBehaviourPunCallbacks {
    [SerializeField] private PlayerInfo playerInfo;
    [SerializeField] private PlayersOverview playersOverview;
    [SerializeField] private CastleHealth castleHealth;
    [SerializeField] private RoundInfo roundInfo;
    [SerializeField] private Image readyButton;
    [SerializeField] private Image surrenderButton;
    private GameController game;

    void Start() {
        if (!PhotonNetwork.LocalPlayer.IsLocal) {
            return;
        }
        // Find the current game
        game = GameObject.Find("gameplay").GetComponent<GameController>();

        // Activate all HUD elements
        foreach (Transform hudElement in transform) {
            hudElement.gameObject.SetActive(true);
        }
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps) {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        try {
            if (changedProps.ContainsKey("gold")) {
                playerInfo.SetGold();
                playersOverview.SetPlayersOverView();

            } else if (changedProps.ContainsKey("ready")) {
                var ready = Helpers.GetPlayerReadyStatus(PhotonNetwork.LocalPlayer);

                Color readyColor;
                Color normalColor;
                ColorUtility.TryParseHtmlString("#51FF47", out readyColor);
                ColorUtility.TryParseHtmlString("#C0FFBD", out normalColor);

                readyButton.color = ready ? readyColor : normalColor;
            }
        } catch {}
    }

    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable changedProps) {
        base.OnRoomPropertiesUpdate(changedProps);
        try {
            if (changedProps.ContainsKey("castleHealth")) {
                changedProps.TryGetValue("castleHealth", out var health);
                game.CastleHealth = (float)health;
                castleHealth.SetCastleHealthText((float)health);

            }
            else if (changedProps.ContainsKey("round")) {
                changedProps.TryGetValue("round", out var round);
                game.Round = (int)round;
                roundInfo.SetRoundText((int)round);

            }
            else if (changedProps.ContainsKey("phase")) {
                changedProps.TryGetValue("phase", out var phase);

                switch((string)phase) {
                    case "InitState": game.GameState =  game.InitState; break;
                    case "PreparationState": game.GameState =  game.PreparationState; break;
                    case "BattleState": game.GameState =  game.BattleState; break;
                    case "WinState": game.GameState = game.WinState; break;
                    case "LoseState": game.GameState = game.LoseState; break;
                    case "EndState": game.GameState = game.EndState; break;
                    default: game.GameState = game.InitState; break;
                }
                roundInfo.SetPhaseText((string)phase);

            }
        } catch {
            Debug.LogError("error: " + changedProps.ToString());
        }
    }

    public void OnReadyButton() {
        if (!PhotonNetwork.LocalPlayer.IsLocal) return;

        bool ready = Helpers.GetPlayerReadyStatus(PhotonNetwork.LocalPlayer);
        Helpers.SetCustomPlayerAttribute(PhotonNetwork.LocalPlayer, "ready", !ready);
    }

    public void OnSurrenderButton() {
        if (!PhotonNetwork.IsMasterClient) return;

        game.GameState = game.LoseState;
    }
}
