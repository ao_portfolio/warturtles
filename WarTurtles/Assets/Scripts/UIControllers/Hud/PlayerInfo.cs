using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour {

    public RawImage classIcon;
    public TMP_Text playerName;
    public TMP_Text gold;

    public void Start() {
        SetClassIcon();
        SetPlayerName();
        SetGold();
    }
    private void SetClassIcon() {
        classIcon.texture = Helpers.GetPlayerClassIcon(PhotonNetwork.LocalPlayer);
    }
    private void SetPlayerName() {
        playerName.text = Helpers.GetPlayerName(PhotonNetwork.LocalPlayer);
    }

    public void SetGold() {
        gold.text = "Gold: " + Helpers.GetPlayerGold(PhotonNetwork.LocalPlayer);
    }
}
