using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class UnitSelector : MonoBehaviour {
    private TDPlayerController player;

    private void Start() {
        player = Helpers.GetPlayerController(PhotonNetwork.LocalPlayer);

        foreach (Transform unitSelectorButton in transform) {
            Button button = unitSelectorButton.GetComponent<Button>();
            RawImage image = unitSelectorButton.GetComponent<RawImage>();
            string classPath = unitSelectorButton.name.Contains("specialUnit") ? player.GetPlayerClass().ToString() : "DEFAULT";
            Texture icon = Resources.Load($"Icons/Units/{classPath}/{unitSelectorButton.name}", typeof(Texture)) as Texture;

            button.onClick.AddListener(delegate { player.SetSelectedUnit(unitSelectorButton.name); });
            image.texture = icon;
        }
    }

    // Update is called once per frame
    private void Update() {
        
    }
}
