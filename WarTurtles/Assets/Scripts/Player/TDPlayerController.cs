using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Linq;
using UnityEngine.SceneManagement;
using TMPro;
using Photon.Realtime;

public class TDPlayerController : MonoBehaviour, IPunObservable {
    private GameController game;
    [SerializeField] private CameraController   playerCamera;
    [SerializeField] private MinimapController  playerMinimap;
    [SerializeField] private SpriteController   playerSprite;
    [SerializeField] private AudioListener      playerAudio;
    [SerializeField] private HudController      playerHud;
    [SerializeField] private Transform          selectedUnit;
    [SerializeField] private Transform          selectedSpell;
    [SerializeField] private Transform          selectedPath;

    [SerializeField] private List<GameObject>   units; // list of all units the player can place, depends on playerClass
    [SerializeField] private List<GameObject>   spells; // list of all spells the player can use, depends on playerClass

    private PhotonView  view;
    private Color       playerColor;
    private PlayerClass playerClass = PlayerClass.DEFAULT;
    private int         gold;


    private void Start() {
        // Playersprites are visible to everyone
        playerSprite.enabled = true;

        // Group all players in hierarchy
        transform.SetParent(GameObject.Find("Players").transform);
        
        // Spawn player in spawn area
        var spawnArea = GameObject.Find("SpawnArea").GetComponent<BoxCollider>().bounds;
        transform.position = new Vector3(spawnArea.center.x, spawnArea.max.y, spawnArea.center.z);

        view = GetComponent<PhotonView>();

        // Set player variables
        name        = Helpers.GetPlayerName(view.Owner); // Set Game Object name to username for Unity inspector
        playerColor = Helpers.GetPlayerColor(view.Owner);
        playerClass = Helpers.GetPlayerClass(view.Owner);
        gold        = 100;

        if (!view.IsMine) return;
        SetUnits();
        SetSpells();

        // Search current game
        game = GameObject.Find("gameplay").GetComponent<GameController>();

        // Search player controllers
        playerAudio     = GetComponent<AudioListener>();
        playerCamera    = GetComponentInChildren<CameraController>();
        playerMinimap   = GetComponentInChildren<MinimapController>();
        playerSprite    = GetComponentInChildren<SpriteController>();
        playerHud       = GetComponentInChildren<HudController>();
        
        // Enable only current player controllers

        playerCamera.enabled    = true;
        playerMinimap.enabled   = true;
        playerAudio.enabled     = true;
        playerHud.enabled       = true;

        // Wait for player to spawn because pathController needs the pathSelector buttons wich are located in the players HUD
        GameObject.Find("Paths").GetComponentInChildren<PathController>().enabled = true;

        Helpers.SetCustomPlayerAttribute(view.Owner, "curScn", SceneManager.GetActiveScene().name);
    }

    // Update is called once per frame
    void Update() {
        if (!view.IsMine) return; 
        if (Input.GetMouseButtonDown(0)) {
            if (EventSystem.current.IsPointerOverGameObject()) return;              // UI click
            if (game.GameState == null) return;
            if (game.GameState != game.PreparationState) return;                    // Can only place units in preperation state
            if (selectedUnit == null) return;                                       // No unit selected
            if (gold < selectedUnit.GetComponent<Unit>().price) return;             // Not enough gold for unit
            PlaceUnit(selectedUnit);
        } 
        else if (Input.GetMouseButtonDown(1)) {
            if (EventSystem.current.IsPointerOverGameObject()) return;              // UI click
            if (game.GameState == null) return;
            if (game.GameState != game.BattleState) return;                         // Can only use spells in battle state
            if (selectedSpell == null) return;                                      // No spell selected
            if (gold < selectedSpell.GetComponent<Smokescreen>().price) return;     // Not enough gold for spell
            UseSpell(selectedSpell);
        }
    }

    #region Getters
    public PhotonView GetView() {
        return view;
    }

    public Color GetColor() {
        return playerColor;
    }

    public PlayerClass GetPlayerClass() {
        return playerClass;
    }

    public Transform GetCamera() {
        return transform.Find("PlayerCamera").transform;
    }

    public SpriteController GetSprite() {
        return playerSprite;
    }

    public Transform GetSelectedUnit() {
        return selectedUnit;
    }

    public Transform GetSelectedPath() {
        if (selectedPath == null) {
            selectedPath = GameObject.Find("Paths").transform.GetChild(0);
        }
        return selectedPath;
    }
    #endregion

    #region Setters
    public void SetSelectedPath(Transform path) {
        selectedPath = path;
    }

    public void SetSelectedUnit(string unitType) {
        try {
            Debug.Log("Selecting new unit: " + unitType, gameObject);
            var unit = GetUnitFromPlayerClass(unitType);
            selectedUnit = unit;
        } catch {
            selectedUnit = null;
        }
    }

    public void SetSelectedSpell(string spellType) {
        try {
            Debug.Log("Selecting new spell: " + spellType, gameObject);
            var spell = GetSpellFromPlayerClass(spellType);
            selectedSpell = spell;
        }
        catch {
            selectedSpell = null;
        }
    }

    private void SetUnits() {
        units = new List<GameObject>(Resources.LoadAll<GameObject>("Units/DEFAULT")); // Load all default units
        var specialUnits = new List<GameObject>(Resources.LoadAll<GameObject>("Units/" + playerClass)); // Load special unit for the current player class
        units.AddRange(specialUnits);
    }

    private void SetSpells() {
        spells = new List<GameObject>(Resources.LoadAll<GameObject>("Spells/DEFAULT")); // Load all default units
        var specialSpells = new List<GameObject>(Resources.LoadAll<GameObject>("Spells/" + playerClass)); // Load special unit for the current player class
        spells.AddRange(specialSpells);
    }
    #endregion

    private void PlaceUnit(Transform unit) {
        Ray mouseWorldPosition = playerCamera.gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        LayerMask terrainMask = 1 << 3;

        // Check if mouse is over terrain
        if (Physics.Raycast(mouseWorldPosition, out RaycastHit hit, 1000f, terrainMask)) {
            // Check if position is in spawnable area
            if (CheckSpawnable(hit.point)) {
                RemoveGold(unit.GetComponent<Unit>().price);

                // Spawn unit over Photon network
                var newUnit = PhotonNetwork.Instantiate(
                    "units/" + (unit.name.Contains("specialUnit") ? playerClass.ToString() : "DEFAULT") + "/" + unit.name,  // Units/CLASS/unitType
                    hit.point + Vector3.up * unit.transform.localScale.y / 2,                                               // position
                    Quaternion.identity);                                                                                   // rotation

                newUnit.transform.SetParent(GameObject.Find("Units").transform);
            }
        }
    }

    private void UseSpell(Transform spell) {
        Ray mouseWorldPosition = playerCamera.gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        LayerMask terrainMask = 1 << 3;

        // Check if mouse is over terrain
        if (Physics.Raycast(mouseWorldPosition, out RaycastHit hit, 1000f, terrainMask)) {
            RemoveGold(spell.GetComponent<Smokescreen>().price);

            // Spawn spell over Photon network
            PhotonNetwork.Instantiate(
                "spells/DEFAULT/" + spell.name,  
                hit.point + Vector3.up * spell.transform.localScale.y / 2,                                                // position
                Quaternion.identity);                                                                                     // rotation
        }
    }



    private void RemoveGold(int price) {
        gold -= price;
        Helpers.SetCustomPlayerAttribute(view.Owner, "gold", gold);
    }

    [PunRPC]
    public void RPC_GiveGold(int amount) {
        gold += amount;
        Helpers.SetCustomPlayerAttribute(view.Owner, "gold", gold);
    }

    private Transform GetUnitFromPlayerClass(string unitType) {
        return units.Find(unit => unit.name.Contains(unitType)).transform;
    }

    private Transform GetSpellFromPlayerClass(string spellType) {
        return spells.Find(spell => spell.name.Contains("Smokescreen")).transform;
    }

    private bool CheckSpawnable(Vector3 position) {
        BoxCollider spawnArea = GameObject.Find("SpawnArea").GetComponent<BoxCollider>();
        return spawnArea.bounds.Contains(position);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            stream.SendNext(GetSelectedPath().name);
        } else if (stream.IsReading) { 
            selectedPath = GameObject.Find("Paths").transform.Find(stream.ReceiveNext().ToString());
        }
    }
}
