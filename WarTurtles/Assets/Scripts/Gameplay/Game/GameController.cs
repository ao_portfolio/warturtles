using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Linq;
using Photon.Pun.UtilityScripts;
using System;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviourPunCallbacks {
    public GameObject playerPrefab;

    public TDPlayerController[] Players;
    public Player[] PlayersInRoom;
    public UnitController[] Units;
    public Dictionary<TowerLocation, Tower> Towers;

    public const int MAX_ROUNDS = 10;

    public string RoomName;
    public string Map;
    public int Round;
    public float CastleHealth;
    public bool RoundOver;
    public bool GameOver;

    public RoundTimer GameTimer;
    public GameChatController Chat;

    public IGameState GameState;
    public InitState InitState;
    public PreparationState PreparationState;
    public BattleState BattleState;
    public WinState WinState;
    public LoseState LoseState;
    public EndState EndState;

    private IEnumerator LoadGame() {
        try {
            GetPlayers();
            InvokeRepeating("GetUnits", 0, 1);

            // Only for debugging
            if (!PhotonNetwork.IsConnected) {
                Connect();
            }
            else {
                SpawnPlayer();
            }

            // Master client controls the game
            if (!PhotonNetwork.IsMasterClient) yield break;

            GameState = InitState;
        }
        catch (Exception e){
            Debug.LogError("Error: " + e.Message, gameObject);
        }
    }

    private void Start() {
        InitState = new InitState();
        PreparationState = new PreparationState();
        BattleState = new BattleState();
        WinState = new WinState();
        LoseState = new LoseState();
        EndState = new EndState();

        Debug.Log("Loading game");
        StartCoroutine(LoadGame());
    }

    private void Update() {
        if (GameState == null) return;
        if (!PhotonNetwork.IsMasterClient) return;
        GameState = GameState.DoState(this);
    }

    public TDPlayerController[] GetPlayers() {
        Players = GameObject.Find("Players").GetComponentsInChildren<TDPlayerController>();
        return Players;
    }

    public void GetUnits() {
        Units = GameObject.Find("Units").GetComponentsInChildren<UnitController>();
    }

    public void SpawnPlayer() {
        PhotonNetwork.Instantiate(playerPrefab.name, Vector3.zero, Quaternion.identity);
    }

    public void GiveGold(Player to, int amount) {
        var playerView = Helpers.GetPlayerController(to).GetView();
        playerView.RPC("RPC_GiveGold", to, amount);
    }

    public void GiveGold(int amount) {
        foreach (var player in PlayersInRoom) {
            GiveGold(player, amount);
        }
    }

    public void SpawnTowers() {
        const int TOWERS_PER_ROUND = 6;
        Debug.Log($"Spawning Towers (now at {Towers.Values.Where(v => v != null).Count()})");
        // Look for the towers already spawned and the towers that still need to be spawned
        // Checks for every towerLocation if it can find a value for Tower
        var spawnedTowers = new Dictionary<TowerLocation, Tower>();
        var nonSpawnedTowers = new Dictionary<TowerLocation, Tower>();

        foreach (KeyValuePair<TowerLocation, Tower> pair in Towers) {
            if (pair.Value == null) {
                nonSpawnedTowers.Add(pair.Key, pair.Value);
            } else {
                spawnedTowers.Add(pair.Key, pair.Value);
            }
        }

        int towersToSpawn = nonSpawnedTowers.Count() < TOWERS_PER_ROUND ? nonSpawnedTowers.Count() : TOWERS_PER_ROUND; // Spawn a number of towers or less if needed
        var randomLocationIndices = new HashSet<int>();

        // As long as there isn't a locationIndex found for each tower to spawn, get new index
        // Use HashSet so duplicates are removed
        while (randomLocationIndices.Count() < towersToSpawn) {
            int randomLocationIndex = UnityEngine.Random.Range(0, nonSpawnedTowers.Count()); // Choose random location for tower
            randomLocationIndices.Add(randomLocationIndex);
        }

        // Find the corresponding locations from the selected indices
        var towerList = GameObject.Find("Towers").transform;

        foreach (var index in randomLocationIndices) {
            var tower = nonSpawnedTowers.ElementAt(index);
            string towerName = tower.Key.startTower.ToString() + " Tower";

            Vector3 positionOffset = new Vector3(0, -1.5f, 0);
            Vector3 towerPosition = tower.Key.transform.position + positionOffset;

            Quaternion towerRotation = Quaternion.identity;

            var newTower = PhotonNetwork.Instantiate($"Towers/{towerName}", towerPosition, towerRotation);
            newTower.transform.SetParent(towerList);

            var component = newTower.GetComponent(typeof(Tower)) as Tower;
            Towers[tower.Key] = component;
        }
    }

    public void ActivateTowers() {
        foreach (Tower tower in Towers.Values) {
            if (tower != null) {
                tower.disabled = false;
            }
        }
    }

    public void DeactivateTowers() {
        foreach (Tower tower in Towers.Values) {
            if (tower != null) {
                tower.disabled = true;
            }
        }
    }

    public void ActivateUnits() {
        foreach (UnitController unit in Units) {
            unit.Activate();
        }
    }

    public void DeactivateUnits() {
        foreach (UnitController unit in Units) {
            unit.Deactivate();
        }
    }

    public void SendGameInfo(IGameState gameState) {
        Helpers.SetCustomRoomAttribute("round", Round);
        Helpers.SetCustomRoomAttribute("phase", gameState.ToString());
        Helpers.SetCustomRoomAttribute("castleHealth", CastleHealth);
    }

    public void OnTimerEnded() {
        RoundOver = true;
    }

    public void OnGameEnded(bool won) {
        if (GameOver) {
            foreach(var player in PlayersInRoom) {
                Helpers.SetCustomPlayerAttribute(player, "curScn", "");
            }
            StartCoroutine(PostGame(won));
        }
    }

    public IEnumerator PostGame(bool won) {
        GameOver = false;

        var postGameScreen = transform.Find("PostGame");
        postGameScreen.GetComponent<Canvas>().enabled = true;
        postGameScreen.Find("PostGameText").GetComponentInChildren<TMP_Text>().text = won ? "Congratulations!\n You have succesfully infiltrated the enemy castle." : "Mission failed,\n better luck next time";
        postGameScreen.Find("PostGameInfo").GetComponentInChildren<TMP_Text>().text = "Press Y to start another game. (Your party will be kept)\nPress N to return to the main lobby";
       
        yield return new WaitUntil(() => {
            if (Input.GetKeyDown(KeyCode.Y)) {
                return BackToWaitingRoom(); // ReloadGame()
            }
            else if (Input.GetKeyDown(KeyCode.H)) {
                return BackToWaitingRoom();
            }
            else if (Input.GetKeyDown(KeyCode.N)) {
                return BackToLobby();
            }
            return false;
        });
        
        GameOver = true;
    }

    private bool ReloadGame() {
        return true;
    }

    private bool BackToWaitingRoom() {
        try {
            PhotonNetwork.LoadLevel("WaitingRoom");
        } catch {
            PhotonNetwork.Disconnect();
            SceneManager.LoadScene("MainMenu");
        }
        return true;
    }

    private bool BackToLobby() {
        try {
            PhotonNetwork.LoadLevel("LobbyRoom");
        }
        catch {
            PhotonNetwork.Disconnect();
            SceneManager.LoadScene("MainMenu");
        }
        return true;
    }

    public override void OnPlayerLeftRoom(Player otherPlayer) {
        base.OnPlayerLeftRoom(otherPlayer);
        PlayersInRoom = PhotonNetwork.PlayerList;
        GetPlayers();
    }


    #region DEBUG
    // TEMP
    public void Connect() {
        PhotonNetwork.LocalPlayer.SetCustomProperties(CustomPlayerAttributes());
        PhotonNetwork.NickName = "Unity Editor" + PhotonNetwork.CountOfPlayers;
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster() {
        base.OnConnectedToMaster();
        Photon.Realtime.RoomOptions roomOptions = new Photon.Realtime.RoomOptions();
        roomOptions.MaxPlayers = 4;
        roomOptions.EmptyRoomTtl = 0;
        roomOptions.PlayerTtl = 0;
        PhotonNetwork.JoinOrCreateRoom(RoomName == "" ? "TESTING" : RoomName, roomOptions, Photon.Realtime.TypedLobby.Default);
    }

    public override void OnJoinedRoom() {
        base.OnJoinedRoom();
        if (!PhotonNetwork.IsMasterClient) {
            return;
        }
        foreach (var player in PhotonNetwork.PlayerList) {
            Helpers.SetCustomPlayerAttribute(player, "gold", 100);
            Helpers.SetCustomPlayerAttribute(player, "ready", false);
        }
        Helpers.SetCustomRoomAttribute("castleHealth", 100f);
        Helpers.SetCustomRoomAttribute("round", 1);
        Helpers.SetCustomRoomAttribute("phase", InitState.ToString());

        SpawnPlayer();
    }

    private ExitGames.Client.Photon.Hashtable CustomPlayerAttributes() {
        ExitGames.Client.Photon.Hashtable playerAttributes = new ExitGames.Client.Photon.Hashtable();

        playerAttributes.Add("color", "RGBA(1.000, 1.000, 1.000, 1.000)");
        playerAttributes.Add("ready", false);
        playerAttributes.Add("class", "MAGE");
        playerAttributes.Add("gold", 100);

        return playerAttributes;
    }
    #endregion
}
