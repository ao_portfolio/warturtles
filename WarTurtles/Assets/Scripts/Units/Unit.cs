using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public float speed = 10f;
    public float health = 10f;
    public float damage = 1f;
    private int _poisonCount = 0;
    public int price;
    public bool targetable = true;
    void Update()
    {
        //transform.Translate(Vector3.forward * Time.deltaTime * speed/20);
    }

    public void Hit(float damage)
    {
        //if (!PhotonNetwork.IsMasterClient) return;
        health -= damage;
        if (health <= 0)
        {
            PhotonNetwork.Destroy(gameObject);
        }
    }

    public void PoisonHit()
    {
        if(_poisonCount <= 0) InvokeRepeating(nameof(PoisonDamage),1, 1);
        _poisonCount = 10;
        transform.Find("Poison effect").gameObject.SetActive(true);
    }

    private void PoisonDamage()
    {
        Hit(1);
        _poisonCount--;
        if(_poisonCount <= 0)
        {
            CancelInvoke(nameof(PoisonDamage));
            transform.Find("Poison effect").gameObject.SetActive(false);
        }
    }
}
