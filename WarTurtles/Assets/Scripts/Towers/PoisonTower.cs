using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PoisonTower : Tower
{
    private Projectile decorProjectile;
    protected override void Start()
    {
        base.Start();
        decorProjectile = Instantiate(projectile, projectile.transform.position, projectile.transform.rotation, partToRotate.transform);
        decorProjectile.GetComponent<Rigidbody>().isKinematic = true;
    }
    protected override void Shoot()
    {
        if (target != null && !disabled) {
            decorProjectile.gameObject.SetActive(false);
        }
        base.Shoot();
        Invoke("EnableProjectile", 1);
    }

    private void EnableProjectile()
    {
        decorProjectile.gameObject.SetActive(true);
    }
}
