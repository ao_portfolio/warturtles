using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseState : IGameState {
    public IGameState DoState(GameController game) {
        game.GameOver = true;
        game.OnGameEnded(false);
        return game.EndState;
    }

    public override string ToString() {
        return "LoseState";
    }
}
