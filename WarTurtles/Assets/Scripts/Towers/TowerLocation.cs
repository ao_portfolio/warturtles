using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerLocation : MonoBehaviour
{
    private float scale = 1.5f;

    public TowerType startTower = TowerType.None;

    private void OnDrawGizmos()
    {    
        Gizmos.color = Color.gray;
        Gizmos.DrawWireMesh(GetComponent<MeshFilter>().sharedMesh, transform.position, transform.rotation, new Vector3(scale, scale, scale));
    }
}
