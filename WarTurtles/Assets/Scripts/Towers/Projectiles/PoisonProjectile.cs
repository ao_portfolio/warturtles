using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonProjectile : Projectile
{
    protected override void Start()
    {
        base.Start();
        rb.centerOfMass = new Vector3(0, 0.2f, 0);
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        if (collision.collider != null && collision.collider.transform.parent.CompareTag("Unit"))
        {
            collision.collider.transform.parent.GetComponent<Unit>().PoisonHit();
            transform.parent = collision.body.transform;
        }
        else
        {
            transform.parent = transform.parent.parent;
        }

        GetComponent<Collider>().enabled = false;
        rb.isKinematic = true;
        Invoke("DestroyObject", 2);
    }
    private void DestroyObject()
    {
        PhotonNetwork.Destroy(gameObject);
    }
}
