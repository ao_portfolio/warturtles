using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UnitSpawner : MonoBehaviour
{
    public Unit unit;
    public GameObject frame_1;
    public bool selected = true;
    // Start is called before the first frame update
    void Start()
    {
        //Spawn();  //-> makes a troop spawn on start
        //Button frame1 = frame_1.GetComponent<Button>();
        //selected = false;
		//frame1.onClick.AddListener(TaskOnClick);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space) && selected)
        {
            Spawn();
        }
    }

    private void Spawn()
    {
        var newUnit = Instantiate(unit.gameObject, transform.position, transform.rotation, transform);
        newUnit.gameObject.SetActive(true);
    }

    // Sets selected on true -> troops can spawn
    void TaskOnClick(){
        // selected = true;
        selected = !selected;
	}
}
