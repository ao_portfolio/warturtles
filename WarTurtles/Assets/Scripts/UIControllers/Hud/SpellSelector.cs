using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class SpellSelector : MonoBehaviour {
    private TDPlayerController player;

    private void Start() {
        player = Helpers.GetPlayerController(PhotonNetwork.LocalPlayer);

        foreach (Transform spellSelectorButton in transform) {
            Button button = spellSelectorButton.GetComponent<Button>();
            RawImage image = spellSelectorButton.GetComponent<RawImage>();
            string classPath = spellSelectorButton.name.Contains("specialSpell") ? player.GetPlayerClass().ToString() : "DEFAULT";
            Texture icon = Resources.Load($"Icons/Spells/{classPath}/{spellSelectorButton.name}", typeof(Texture)) as Texture;

            button.onClick.AddListener(delegate { player.SetSelectedSpell(spellSelectorButton.name); });
            image.texture = icon;
        }
    }
}
