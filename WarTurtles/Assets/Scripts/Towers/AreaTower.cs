using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AreaTower : Tower
{
    protected override void Shoot()
    {
        if (target != null && !disabled && target.GetComponent<Unit>().targetable)
        {           
            var newProjectile = PhotonNetwork.Instantiate("Towers/Projectiles/" + projectile.GetType(), projectile.transform.position, projectile.transform.rotation);
            newProjectile.transform.SetParent(partToRotate.transform);
            newProjectile.GetComponent<Projectile>().damage = attackDamage;
            newProjectile.GetComponent<Projectile>().force *= TargetDistance();
            StartCoroutine(DelayedSetActive(newProjectile.GetComponent<Projectile>()));
        }
        else if(_unitsInRange.Count > 0 && !disabled)
        {
            _unitsInRange = _unitsInRange.Where(item => item != null).ToList();
            NextTarget();
        }
    }
    private IEnumerator DelayedSetActive(Projectile newProjectile)
    {
        var temp = newProjectile.transform.parent; //removed parent temporarely to prevent unwanted rotation
        newProjectile.transform.parent = null;
        yield return new WaitForSeconds(2f); //added delay for inaccuracy (on purpose)
        if (newProjectile == null) yield break;
        newProjectile.transform.parent = temp;
        newProjectile.gameObject.SetActive(true);
    }
}
