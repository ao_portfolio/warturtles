using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpriteController : MonoBehaviour{
    [SerializeField] private Vector3 offset = new Vector3(0, 81, 0);
    private TDPlayerController player;

    private void Start() {
        player = gameObject.GetComponentInParent<TDPlayerController>();
        SetSpriteColor(player.GetColor());
    }
    private void LateUpdate() {
        if (player.GetView().IsMine) {
            var playerCamera = player.GetCamera();
            transform.position = playerCamera.position + offset;
            transform.rotation = Quaternion.Euler(90, playerCamera.eulerAngles.y, 0);
        }
    }

    public void SetSpriteColor(Color playerColor) {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = true;
        spriteRenderer.color = playerColor;
    }
}
