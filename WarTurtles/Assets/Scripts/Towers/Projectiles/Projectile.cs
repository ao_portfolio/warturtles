using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    public Rigidbody rb;
    public float force = 100;
    public float damage = 1f;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        rb.AddRelativeForce(0,0, force);
        transform.position = GetComponentInParent<Transform>().position;
    }

    protected abstract void OnCollisionEnter(Collision collision);
}
