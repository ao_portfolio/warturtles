public class BattleState : IGameState {
    public IGameState DoState(GameController game) {
        if (game.CastleHealth <= 0) {
            game.SendGameInfo(game.WinState);
            return game.WinState;
        }

        if (game.RoundOver) {
            game.RoundOver = false;
            game.Round++;
            game.SendGameInfo(game.PreparationState);
            game.SpawnTowers();
            game.DeactivateTowers();
            game.DeactivateUnits();

            return game.PreparationState;
        }
        return game.BattleState;
    }

    public override string ToString() {
        return "BattleState";
    }
}
