using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {
    public float movementSpeed { get; set; }
    [SerializeField] private float stepHeight = 2f;
    [SerializeField] private float stepSmooth = 0.1f;
    private Rigidbody rb;

    private void Start() {
        rb = GetComponent<Rigidbody>();
        movementSpeed = 10f;
    }

    public void MoveTo(Vector3 position) {
        var targetHeight = GetGroundHeight(position);
        var targetPosition = new Vector3(position.x, targetHeight, position.z);

        var direction = (targetPosition - transform.position).normalized;

        rb.MovePosition(transform.position + (direction * movementSpeed * Time.deltaTime));
        RotateTowards(direction);
    }

    private void RotateTowards(Vector3 target) {
        var lookRotation = Quaternion.LookRotation(target);
        var rotation = Quaternion.Lerp(transform.rotation, lookRotation, movementSpeed * Time.deltaTime).eulerAngles;

        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    private float GetGroundHeight(Vector3 position) {
        Physics.Raycast(position, Vector3.down, out RaycastHit hitInfo, 500, 1 << 3);
        if (hitInfo.transform == null) {
            return -1;
        }
        return hitInfo.transform.position.y;
    }
}
