using ExitGames.Client.Photon;
using Photon.Chat;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameChatController : MonoBehaviour, IChatClientListener {
    [SerializeField] private Canvas chatWindow;
    [SerializeField] private TMP_InputField chatInput;
    [SerializeField] private Text chatHistory;

    private ChatClient chatClient;
    private string channel;


    void Start() {
        // Closes chat on inital load
        chatWindow.enabled = false;
        chatInput.text = "";
        chatHistory.text = "";
    }

    void Update() {
        // Wait for chatClient to load
        if(chatClient == null) {
            return;
        }
        // Maintain connection to chat
        chatClient.Service();

        // Opens chat when t key is pressed
        // Sends message when enter is pressed
        if (Input.GetKeyUp(KeyCode.T)) {
            OpenChat();
        } else if (Input.GetKeyUp(KeyCode.Return)) {
            SendChat();
        }
    }

    public void InitChat() {
        // Connects to PhotonChat and calls the OnConnected callback
        chatClient = new ChatClient(this);
        chatClient.Connect(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat, PhotonNetwork.AppVersion, new AuthenticationValues(PhotonNetwork.LocalPlayer.NickName));
    }

    public void SendChat(string info = "") {
        // Sends a message to all players in the room
        // If info is sent, such as player joined/left,
        // only show it to other players and don't save the message in chatlogs
        string message = chatInput.text;
        if (!string.IsNullOrEmpty(info)) {
            chatHistory.text += info + "\n";
            return;
        }
        if (string.IsNullOrEmpty(message) || !chatWindow.enabled) {
            return;
        }
        chatInput.text = "";
        chatInput.Select();
        chatInput.ActivateInputField();

        chatClient.PublishMessage(channel, message);
    }

    private void OpenChat() {
        chatWindow.enabled = true;
        chatInput.Select();
        chatInput.ActivateInputField();
    }

    public void CloseChat() {
        // Is called in the OnDeselect event of chatInput
        chatInput.DeactivateInputField();
        chatWindow.enabled = false;
    }

    public void LeaveChat() {
        chatClient.Unsubscribe(new[] { channel });
    }

    #region PhotonChatCallbacks

    public void DebugReturn(DebugLevel level, string message) {
    
    }

    public void OnChatStateChange(ChatState state) {
        //Debug.Log("Chatstate: " + state);
    }

    public void OnConnected() {
        // Joins the chat channel of the current room
        channel = PhotonNetwork.CurrentRoom?.Name;

        if (channel == null) {
            InitChat();
            SendChat("Error");
        }

        ChannelCreationOptions channelOptions = new ChannelCreationOptions();
        channelOptions.PublishSubscribers = true;
        channelOptions.MaxSubscribers = 4;
        
        chatClient.Subscribe(channel, 0, -1, channelOptions);
    }

    public void OnDisconnected() {
        //Debug.Log("Chat disconnected");
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages) {
        // If message is from current channel (waiting room)
        if (channelName == channel) {
            if (messages.Length == 1) {
                chatHistory.text += senders[0] + ": " + messages[0] + "\n";
            } else {
                chatHistory.text = "";
                for (int i = 0; i < messages.Length; i++) {
                    chatHistory.text += senders[i] + ": " + messages[i] + "\n";
                }
            }
        }
    }

    public void OnPrivateMessage(string sender, object message, string channelName) {
    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message) {
    }

    public void OnSubscribed(string[] channels, bool[] results) {
    }

    public void OnUnsubscribed(string[] channels) {
        chatClient.Disconnect();
    }

    public void OnUserSubscribed(string channel, string user) {
        if (channel == this.channel) {
            string info = user + " has joined the game";
            SendChat(info);
        }
    }

    public void OnUserUnsubscribed(string channel, string user) {
        if (channel == this.channel) {
            string info = user + " has left the game";
            SendChat(info);
        }
    }

    #endregion
}
