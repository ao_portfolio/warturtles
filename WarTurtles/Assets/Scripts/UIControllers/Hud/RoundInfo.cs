using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RoundInfo : MonoBehaviour {
    [SerializeField] private TMP_Text roundText;
    [SerializeField] private TMP_Text phaseText;

    public void SetRoundText(int round) {
        roundText.text = "Round: " + round;
    }

    public void SetPhaseText(string phase) {
        phaseText.text = phase.Replace("State", " Phase");
    }
}
