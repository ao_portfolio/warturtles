using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Waypoint : MonoBehaviour, IPunObservable {
    [SerializeField] private bool isCheckpoint = false;
    [SerializeField] private float waypointRange = 5f;
    [SerializeField] private SpriteRenderer sprite; // MinimapSprite to view checkpoints on minimap

    private GameController game;

    public bool IsCheckpoint { get => isCheckpoint; set => isCheckpoint = value; }
    public float WaypointRange { get => waypointRange; private set => waypointRange = value; }

    private void Start() {
        GetComponent<CapsuleCollider>().radius = waypointRange;
        game = GameObject.Find("gameplay").GetComponent<GameController>();
    }

    private void Update() {
        sprite.enabled = isCheckpoint;
    }

    private void OnTriggerEnter(Collider other) {
        if (!PhotonNetwork.IsMasterClient) return;
        if (other.CompareTag("Unit")) {
            if (isCheckpoint) {
                isCheckpoint = false;
                game.InitState.SetRandomCheckpoint();

                game.GiveGold(10);
            } else {
                var unit = other.transform.parent.gameObject;
                var unitController = unit.GetComponent<UnitController>();
                var unitOwner = unit.GetPhotonView().Owner;
                var unitPrice = unit.GetComponent<Unit>().price;

                // Get all waypoints from selected path
                var selectedPath = unitController.GetPath();
                var selectedWaypoints = selectedPath?.Cast<Transform>()?.ToList();

                // If this waypoint is in selected path
                if (selectedWaypoints == null) return;
                if (selectedWaypoints.Contains(transform)) {
                    game.GiveGold(unitOwner, Mathf.FloorToInt(unitPrice / 10));
                }
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            stream.SendNext(isCheckpoint);
        } else if (stream.IsReading) {
            isCheckpoint = (bool)stream.ReceiveNext();
        }
    }
}
