using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinState : IGameState {
    public IGameState DoState(GameController game) {
        game.GameOver = true;
        game.OnGameEnded(true);
        return game.EndState;
    }

    public override string ToString() {
        return "WinState";
    }
}
