public enum PlayerClass {
    DEFAULT = -1,
    MAGE = 0,
    WARRIOR = 1,
    ROGUE = 2,
    TANK = 3,
}
