using System.Collections.Generic;
using UnityEngine;
/** 
 * Helper class for viewing waypoints in sceneview
 * Blue spheres represent individual waypoints
 * White lines represent the connection between waypoints
 * 
 * This script must be inside a gameObject containing all waypoints
 * so this.transform is a list of all waypoints
 * 
 * GetNextWaypoint
 *      Gets next waypoint based on the current waypoint
 * 
 * 
 * Inspired by MetalStorm Games
 * 
 * https://www.youtube.com/watch?v=EwHiMQ3jdHw
 */
public class WaypointsController : MonoBehaviour {
    private LineRenderer lineRenderer;

    private void Start () {
        lineRenderer = GetComponent<LineRenderer>();
        DrawWaypoints();
    }

    public Waypoint GetNextWaypoint(Waypoint currentWaypoint) {
        // Starts with the first waypoint
        if (currentWaypoint == null) {
            return transform.GetChild(0).GetComponent<Waypoint>();
        }
        // Next waypoint 
        if (currentWaypoint.transform.GetSiblingIndex() < transform.childCount - 1) {
            var nextWaypoint = transform.GetChild(currentWaypoint.transform.GetSiblingIndex() + 1).GetComponent<Waypoint>();
            return nextWaypoint;
        }
        // No waypoint left
        return null;
    }

    public bool IsInRangeOfWaypoint(Transform unit, Waypoint waypoint) {
        var distanceToWaypoint = GetDistanceToWaypoint(unit, waypoint.transform);
        return distanceToWaypoint <= waypoint.WaypointRange;
    }

    public void Enable() {
        lineRenderer.enabled = true;
    }

    public void Disable() {
        lineRenderer.enabled = false;
    }

    // Draws waypoints to display on minimap
    private void DrawWaypoints() {
        lineRenderer.positionCount = transform.childCount;
        foreach (Transform waypoint in transform) {
            lineRenderer.SetPosition(waypoint.GetSiblingIndex(), waypoint.position);
        }
    }

    private float GetDistanceToWaypoint(Transform unit, Transform waypoint) {
        var unitPosition = new Vector3(unit.position.x, 0, unit.position.z);
        var waypointPosition = new Vector3(waypoint.position.x, 0, waypoint.position.z);
        var distance = Vector3.Distance(unitPosition, waypointPosition);

        return distance;
    }

    #region Debug
    private void OnDrawGizmos() {
        /*
        DebugDrawWaypoints(transform);
        DebugDrawConnections(transform);
        */
    }

    private void DebugDrawWaypoints(Transform waypoints) {
        
        Gizmos.color = Color.cyan;
        foreach (Transform waypoint in waypoints) {
            Gizmos.DrawWireSphere(waypoint.position, 0.5f);

            if (waypoint.childCount >= 1) {
                DebugDrawWaypoints(waypoint);
            }
        }
    }
    private void DebugDrawConnections(Transform waypoints) {
        Gizmos.color = Color.white;
        for (int i = 0; i < waypoints.childCount - 1; i++) {
            var waypoint = waypoints.GetChild(i);
            var nextWaypoint = waypoints.GetChild(i + 1);

            Gizmos.DrawLine(waypoint.position, nextWaypoint.position);

            if (waypoint.childCount >= 1) {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(waypoint.position, nextWaypoint.position);
                Gizmos.DrawLine(waypoint.position, waypoint.GetChild(0).position);

                DebugDrawConnections(waypoint);
            }
        }
    }
    #endregion
}
