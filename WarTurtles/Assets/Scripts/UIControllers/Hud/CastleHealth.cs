﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;



public class CastleHealth : MonoBehaviour
{
    public Slider Slider;
    public Image Fill;
    public TMP_Text CastleHealthText;

    private float castleHealth = 100;

    private void Update() {
        FillSlider();
    }

    private void FillSlider() {
        Fill.fillAmount = castleHealth / 100;
    }

    public void SetCastleHealthText(float castleHealth) {
        this.castleHealth = castleHealth;
        CastleHealthText.text = "Castle Health: " + castleHealth + "%";
    }
}
