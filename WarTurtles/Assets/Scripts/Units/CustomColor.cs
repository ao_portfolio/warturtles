using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomColor : MonoBehaviour
{
    // Start is called before the first frame update
    public Color color = new Color(159, 255, 0, 1);

    private Material[] materials;
    private Material[] materialsSkinned;

    void Start()
    {
        color = Helpers.GetPlayerColor(GetComponent<PhotonView>().Owner); //place in comments to test

        if (transform.Find("model").GetComponentInChildren<MeshRenderer>() != null)
        {
            materials = transform.Find("model").GetComponentInChildren<MeshRenderer>().materials;
        }
        
        if(transform.Find("model").GetComponentInChildren<SkinnedMeshRenderer>() != null)
        {
            materialsSkinned = transform.Find("model").GetComponentInChildren<SkinnedMeshRenderer>().materials;
        }       
        
        
        if (name.Contains("normalUnit"))
        {
            transform.Find("model").Find("Shell").GetComponent<SkinnedMeshRenderer>().materials[0].color = color;
            transform.Find("model").Find("Shoes").GetComponent<SkinnedMeshRenderer>().material.color = color;
        }
        else if (name.Contains("lightUnit" ))
        {
            materialsSkinned[0].color = color;
        }
        else if (name.Contains("heavyUnit"))
        {
            materials[1].color = color;
        }
        else if (name.Contains("specialUnitWizard"))
        {
            materialsSkinned[0].SetColor("_OuterChlothes", color);
        }
        else if (name.Contains("specialUnitCat"))
        {
            materialsSkinned[1].color = color;
            materialsSkinned[2].color = new Color(
                1 - color.r,
                1 - color.g,
                1 - color.b
            ); ;//accent color
        }
        else if (name.Contains("specialUnitWarrior"))
        {
            materials[0].color = color; //sword
            materialsSkinned[2].color = color;//unit

            var darkerColor = new Color(
                color.r / 5,
                color.g / 5,
                color.b / 5
            );
            materialsSkinned[1].color = darkerColor;//accent color unit
        }
        else if (name.Contains("specialUnitOrc"))
        {
            materialsSkinned[1].color = color;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
