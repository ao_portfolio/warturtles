using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTower : Tower
{
    public int slowPercentage = 30;

    // Start is called before the first frame update
    protected override void Start()
    {
        range.radius = attackRange;
    }

    protected override void Update() { }

    private void FixedUpdate()
    {
        transform.GetChild(0).transform.Rotate(Vector3.up, 1);
    }

    protected override void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.parent.CompareTag("Unit"))
        {
            collider.transform.parent.GetComponent<MovementController>().movementSpeed *= (float)(100-slowPercentage)/100;
            collider.transform.parent.Find("Slow effect").gameObject.SetActive(true);
        }
    }

    protected override void OnTriggerExit(Collider collider)
    {
        if (collider.transform.parent.CompareTag("Unit"))
        {
            collider.transform.parent.GetComponent<MovementController>().movementSpeed *= (float)100/(100 - slowPercentage);
            collider.transform.parent.Find("Slow effect").gameObject.SetActive(false);
        }
    }
}
