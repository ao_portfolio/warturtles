using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayersOverview : MonoBehaviour {
    [SerializeField] private GameObject playerIconPrefab;

    // Start is called before the first frame update
    void Start() {
        SetPlayersOverView();
    }

    public void SetPlayersOverView() {
        foreach (Transform icon in transform) {
            Destroy(icon.gameObject);
        }
        foreach (var player in PhotonNetwork.PlayerList) {
            var classIcon = Helpers.GetPlayerClassIcon(player);
            string playerName = Helpers.GetPlayerName(player);
            int playerGold = Helpers.GetPlayerGold(player);

            var playerIcon = Instantiate(playerIconPrefab, transform);
            playerIcon.transform.Find("Name").GetComponentInChildren<TMP_Text>().text = playerName;
            playerIcon.GetComponentInChildren<RawImage>().texture = classIcon;
            playerIcon.transform.Find("Gold").GetComponentInChildren<TMP_Text>().text = "Gold: " + playerGold;
        }
    }
}
