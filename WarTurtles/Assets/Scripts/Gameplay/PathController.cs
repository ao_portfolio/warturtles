using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PathController : MonoBehaviour {
    [SerializeField] private WaypointsController[] paths;
    private Transform pathSelector;
    
    private void Start() {
        paths = GetComponentsInChildren<WaypointsController>();
        pathSelector = GameObject.Find("Path Selector").transform;

        GeneratePathSelectorButtons();
    }

    private void GeneratePathSelectorButtons() {
        foreach (var path in paths) {
            GameObject pathSelectorButton = new GameObject(path.name);
            Image image = pathSelectorButton.AddComponent<Image>();
            Button button = pathSelectorButton.AddComponent<Button>();

            button.onClick.AddListener(delegate { OnSelectedPath(path.name); });

            var buttonNavigation = button.navigation;
            buttonNavigation.mode = Navigation.Mode.Explicit;
            button.navigation = buttonNavigation;

            // Use disabled color so color stays visible when user clicks outside button
            var buttonColors = button.colors;
            buttonColors.disabledColor = Color.cyan;
            button.colors = buttonColors;

            pathSelectorButton.transform.SetParent(pathSelector);
            pathSelectorButton.transform.localScale = Vector3.one;
        }
    }

    public void OnSelectedPath(string selectedPath) {
        foreach (var path in paths) {
            if (path.name == selectedPath) {
                Debug.Log("Path selected: " + selectedPath);
                Helpers.GetPlayerController(PhotonNetwork.LocalPlayer).SetSelectedPath(path.transform);
                path.Enable();
            } else {
                path.Disable();
            }
        }
    }

}
