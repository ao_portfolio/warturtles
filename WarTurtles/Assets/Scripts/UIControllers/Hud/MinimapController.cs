using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapController : MonoBehaviour {
    /** 
     * 
     * */
    [SerializeField] private Vector3 offset = new Vector3(0, 200, -108);
    [SerializeField] private bool rotateMinimap = false;
    private TDPlayerController player;

    private void Start() {
        player = gameObject.GetComponentInParent<TDPlayerController>();
        // Enables minimap camera
        GetComponent<Camera>().enabled = true;
    }

    private void LateUpdate() {
        var playerCamera = player.transform.Find("PlayerCamera").transform;
        transform.position = playerCamera.position + offset;
        if (rotateMinimap) {
            transform.rotation = Quaternion.Euler(90, playerCamera.eulerAngles.y, 0);
        } else {
            transform.rotation = Quaternion.Euler(90, 0, 0);
        }
    }
}
