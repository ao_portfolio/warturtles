using Photon.Pun;
using UnityEngine;

public class UnitController : MonoBehaviour {
    private WaypointsController currentPath;
    private MovementController movementController;
    private Unit unit;
    private Waypoint currentWaypoint;
    private PhotonView view; // Creator of the unit 
    public bool IsActive = true;


    private void Awake() {
        movementController = GetComponent<MovementController>();
        unit = GetComponent<Unit>();
        view = GetComponent<PhotonView>();

        string path;

        try {
            path = Helpers.GetPlayerController(view.Owner).GetSelectedPath().name;
        } catch {
            path = "Path";
        }

        SetPath(path);

        Debug.Log("path: " + path);
    }

    // Start is called before the first frame update
    void Start() {
        transform.parent = GameObject.Find("Units").transform;
        currentWaypoint = currentPath.GetNextWaypoint(currentWaypoint);   
    }

    [PunRPC]
    public void ToggleUnits(bool active) {
        IsActive = active;
    }

    public void Activate() {
        view.RPC("ToggleUnits", RpcTarget.All, true);
    }

    public void Deactivate() {
        view.RPC("ToggleUnits", RpcTarget.All, false);
    }

    private void OnEnable()
    {
        SetWalkingAnimation(true);
    }

    private void OnDisable()
    {
        SetWalkingAnimation(false);
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (currentWaypoint == null) return;
        if (!IsActive) return;
        movementController.movementSpeed = unit.speed / 5;
        movementController.MoveTo(currentWaypoint.transform.position);

        // Get next waypoint if unit is close enough
        if (currentPath.IsInRangeOfWaypoint(transform, currentWaypoint)) {
            currentWaypoint = currentPath.GetNextWaypoint(currentWaypoint);
        }
    }

    public void SetPath(string path = "Path") {
        var paths = GameObject.Find("Paths");
        currentPath = paths.transform.Find(path).GetComponent<WaypointsController>();
    }

    public Transform GetPath() {
        return currentPath.transform;
    }

    public PhotonView GetView() {
        return view;
    }

    public void SetWalkingAnimation(bool value)
    {
        if(GetComponentInChildren<Animator>() != null)
        {
            GetComponentInChildren<Animator>().SetBool("Walking",value);
        }
    }
}
